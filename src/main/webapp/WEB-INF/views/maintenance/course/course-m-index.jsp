<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Courses:</h2>
<c:if test="${not empty statusMessage}">
    <p>
        <span>Status: </span><span>${statusMessage}</span>
    </p>
</c:if>
<a href="${pageContext.request.contextPath}/maintenance/course/add">Create</a>
<table border="1">
    <thead>
        <tr>
            <td></td>
            <td>Course ID</td>
            <td>Course</td>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${courses}" var="course">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><a href="${pageContext.request.contextPath}/maintenance/course/edit/${course.courseId}">Edit</a></td>
                            <td><a href="${pageContext.request.contextPath}/maintenance/course/delete/${course.courseId}">Delete</a></td>
                        </tr>
                    </table>
                </td>
                <td><c:out value="${course.courseId}" /></td>
                <td><c:out value="${course.courseName}" /></td>
            </tr>
        </c:forEach>
    </tbody>
</table>