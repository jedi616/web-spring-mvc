<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:choose>
    <c:when test="${courseForm.courseId ne 0}">
        <h2>Edit Course:</h2>
    </c:when>
    <c:otherwise>
        <h2>Create Course:</h2>
    </c:otherwise>
</c:choose>
<div id="statusMessageContainer" <c:if test="${empty statusMessage}">style="display: none;"</c:if>>
    <p>
        <span>Status: </span><span id="statusMessage">${statusMessage}</span>
    </p>
</div>
<form:form modelAttribute="courseForm" action="${pageContext.request.contextPath}/maintenance/course/save">
    <form:hidden path="courseId" />
    <table border="1">
        <tbody>
            <tr>
                <td>Course</td>
                <td><form:input path="courseName" /></td>
            </tr>
        </tbody>
    </table>
    <a id="saveButton" href="#">Save</a>
    <a href="${pageContext.request.contextPath}/maintenance/course">Cancel</a>
</form:form>