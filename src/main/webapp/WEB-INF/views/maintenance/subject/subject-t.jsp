<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Subjects:</h2>
<c:if test="${not empty statusMessage}">
    <p>
        <span>Status: </span><span>${statusMessage}</span>
    </p>
</c:if>
<table border="1">
    <thead>
        <tr>
            <td>Subject ID</td>
            <td>Subject Name</td>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${subjects}" var="subject">
            <tr>
                <td><c:out value="${subject.subjectId}" /></td>
                <td><c:out value="${subject.subjectName}" /></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<br />
<h2>Add Subjects:</h2>
<table id="subjectTable" border="1">
    <thead>
        <tr>
            <td>Subject Name</td>
        </tr>
    </thead>
    <tbody id="subjectTbody">
    </tbody>
</table>
<button id="addButton" type="button">Add</button>
<button id="saveButton" type="button">Save</button>
<table style="display: none">
    <tbody>
        <tr id="subjectTrTemplate">
            <td><input type="text" /></td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
	var addSubjectCount = 0;

	$(document).ready(function() {
		//initComponents();
		initEventHandlers();
		/*databindComponents();
		initValidators();
		pageLoad(); */
	});

	function initEventHandlers() {
		$("#addButton").click(addButtonClickHandler);
		$("#saveButton").click(saveButtonClickHandler);
	}

	function addButtonClickHandler() {
		addSubjectCount++;
		var subjectTr = $("#subjectTrTemplate").clone().prop("id",
				"newSubject" + addSubjectCount);
		$(subjectTr).prop("id", "newSubject" + addSubjectCount);
		$(subjectTr).prop("name", "newSubject" + addSubjectCount);
		$("#subjectTbody").append(subjectTr);
	}

	function saveButtonClickHandler()
	{
		var subjectArray = [];
        $("#subjectTable tbody tr").each(function(index, object) {
            var input = $(object).find("input[type=text]");
            subjectArray.push({subjectName : $(input).val()})
        });
		$.ajax({
            url: "${pageContext.request.contextPath}/maintenance/subjecttransaction/save",
            data: JSON.stringify(subjectArray),
            type: "POST",
            contentType:'application/json',
            success:  function(data, status, jqXHR) {
            	if (data.status === "SUCCESS") {
                    window.location.href = contextPath + "/maintenance/subjecttransaction";
                }
                else {
                	alert("Insertion Error...");
                }
            },
            error : function(jqXHR, status, error) {
            	alert("ERROR: " + JSON.parse(jqXHR.responseText).message);
            }
        });
	}
</script>