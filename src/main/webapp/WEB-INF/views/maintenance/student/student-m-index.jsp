<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Students:</h2>
<c:if test="${not empty statusMessage}">
    <p>
        <span>Status: </span><span>${statusMessage}</span>
    </p>
</c:if>
<a href="${pageContext.request.contextPath}/maintenance/student/add">Create</a>
<table border="1">
    <thead>
        <tr>
            <td></td>
            <td>Student ID</td>
            <td>Name</td>
            <td>Gender</td>
            <td>Course</td>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${students}" var="student">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><a href="${pageContext.request.contextPath}/maintenance/student/edit/${student.studentId}">Edit</a></td>
                            <td><a href="${pageContext.request.contextPath}/maintenance/student/delete/${student.studentId}">Delete</a></td>
                        </tr>
                    </table>
                </td>
                <td><c:out value="${student.studentId}" /></td>
                <td><c:out value="${student.name}" /></td>
                <td><c:out value="${student.gender}" /></td>
                <td><c:out value="${student.course.courseName}" /></td>
            </tr>
        </c:forEach>
    </tbody>
</table>