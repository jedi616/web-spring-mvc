<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:choose>
    <c:when test="${studentForm.studentId ne 0}">
        <h2>Edit Student:</h2>
    </c:when>
    <c:otherwise>
        <h2>Create Student:</h2>
    </c:otherwise>
</c:choose>
<div id="statusMessageContainer" <c:if test="${empty statusMessage}">style="display: none;"</c:if>>
    <p>
        <span>Status: </span><span id="statusMessage">${statusMessage}</span>
    </p>
</div>
<form:form modelAttribute="studentForm" action="${pageContext.request.contextPath}/maintenance/student/save">
    <form:hidden path="studentId" />
    <table border="1">
        <tbody>
            <tr>
                <td>Name</td>
                <td><form:input path="name" /></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td><form:radiobutton path="gender" value="Male" />Male <form:radiobutton path="gender" value="Female" />Female</td>
            </tr>
            <tr>
                <td>Course</td>
                <td><form:select itemValue="value" itemLabel="label" path="course.courseId" items="${studentForm.courseSelectItems}" />
                </td>
            </tr>
        </tbody>
    </table>
    <a id="saveButton" href="#">Save</a>
    <a href="${pageContext.request.contextPath}/maintenance/student">Cancel</a>
</form:form>