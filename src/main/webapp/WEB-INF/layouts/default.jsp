<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title><tiles:getAsString name="title" /></title>
<tiles:insertAttribute name="defaultScripts" />
<tiles:importAttribute name="customScripts" ignore="true" />
<c:forEach var="customScript" items="${customScripts}" varStatus="status">
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/app${customScript}"></script>
</c:forEach>
<script type="text/javascript">
    var contextPath = "${pageContext.request.contextPath}";
</script>
</head>
<body>
    <table>
        <tr>
            <td><tiles:insertAttribute name="header" /></td>
        </tr>
        <tr>
            <td><tiles:insertAttribute name="navigation" /></td>
        </tr>
        <tr>
            <td><tiles:insertAttribute name="body" /></td>
        </tr>
        <tr>
            <td><tiles:insertAttribute name="footer" /></td>
        </tr>
    </table>
</body>
</html>