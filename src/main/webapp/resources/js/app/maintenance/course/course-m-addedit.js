//Start of On-Ready
$(document).ready(function()
{
    courseMAddEdit.init();
});
// End of On-Ready

var courseMAddEdit = (function()
{
    var init = function()
    {
        initComponents();
        initEventHandlers();
        initValidators();
        pageLoad();
    }

    return {
        init : init
    };

    function initComponents()
    {
        // TODO: Initialize widgets.
    }

    function initEventHandlers()
    {
        $("#saveButton").click(saveButtonClickHandler);
    }

    function initValidators()
    {
        // TODO: Initialize validators.
    }

    function pageLoad()
    {
        // TODO: Initialize page.
    }

    function saveButtonClickHandler()
    {
        $.ajax({
            url : contextPath + "/maintenance/course/save",
            data : $("#courseForm").serialize(),
            type : "POST",
            //global : false, // enable/disable Global Ajax for handling BlockUI.
            success : function(data, status, jqXHR)
            {
                if (data.status === "SUCCESS")
                {
                    window.location.href = contextPath + "/maintenance/course?status_message=" + data.message;
                }
                else
                {
                    $("#statusMessage").text(data.message);
                    $("#statusMessageContainer").show();
                }
            },
            error : function(jqXHR, status, error)
            {
                alert("ERROR: " + JSON.parse(jqXHR.responseText).message);
            }
        });
    }
})();
