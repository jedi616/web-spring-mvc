$(document).ready(function()
{
    // Initialize blockUI.
    $.blockUI.defaults.message = $('<img src="' + contextPath + '/resources/img/common/busy.png"/>');
    $.blockUI.defaults.css.color = "inherit";
    $.blockUI.defaults.css.border = "inherit";
    $.blockUI.defaults.css.backgroundColor = "inherit";
});

// Global Ajax to handle BlockUI.
$(document).ajaxStart(function()
{
    $.blockUI();
}).ajaxStop(function()
{
    $.unblockUI();
}).ajaxError(function()
{
    $.unblockUI();
});
