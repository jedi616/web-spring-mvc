package keith.spring.mvc.controller;

import static keith.domain.util.KeithUtils.encodeMessage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import keith.api.maintenance.service.CourseService;
import keith.domain.dto.CourseDto;
import keith.domain.dto.ServiceResponse;
import keith.domain.dto.UIResponse;
import keith.spring.mvc.validation.CourseDtoValidator;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Controller
@RequestMapping("/maintenance/course")
public class CourseMaintenanceController {
    
    private static final String INDEX_VIEW = "maintenance/course/index";
    private static final String ADD_VIEW = "maintenance/course/add";
    private static final String EDIT_VIEW = "maintenance/course/edit";
    private static final String INDEX_WITH_STATUS_REDIRECT_URL = "redirect:/maintenance/course?status_message=";

    private static final String VALIDATION_ERROR_MESSAGE = "Validation Error";

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseDtoValidator courseDtoValidator;

    @InitBinder("courseForm")
    protected void initBinderCourseForm(WebDataBinder binder) {
        binder.addValidators(courseDtoValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(@RequestParam(value = "status_message", required = false) String statusMessage, Model model)
            throws Exception {
        if (statusMessage != null) {
            model.addAttribute("statusMessage", statusMessage);
        }
        ServiceResponse<List<CourseDto>> serviceResponse = courseService.retrieveAll();
        if (serviceResponse.isSuccess()) {
            model.addAttribute("courses", serviceResponse.getData());
        } else {
            model.addAttribute("statusMessage", serviceResponse.getMessage());
        }
        return INDEX_VIEW;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) throws Exception {
        ServiceResponse<CourseDto> serviceResponse = courseService.retrieveNew();
        if (serviceResponse.isSuccess()) {
            model.addAttribute("courseForm", serviceResponse.getData());
        } else {
            model.addAttribute("statusMessage", serviceResponse.getMessage());
        }
        return ADD_VIEW;
    }

    @RequestMapping(value = "/edit/{courseId}", method = RequestMethod.GET)
    public String edit(@PathVariable("courseId") long courseId, Model model) throws Exception {
        ServiceResponse<CourseDto> serviceResponse = courseService.retrieveOne(courseId);
        if (serviceResponse.isSuccess()) {
            CourseDto courseDto = serviceResponse.getData();
            model.addAttribute("courseForm", courseDto);
            return EDIT_VIEW;
        } else {
            return INDEX_WITH_STATUS_REDIRECT_URL + encodeMessage(serviceResponse.getMessage());
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody UIResponse save(@ModelAttribute("courseForm") @Validated CourseDto courseDto,
            BindingResult bindingResult) throws Exception {
        UIResponse response = new UIResponse();
        if (!bindingResult.hasErrors()) {
            ServiceResponse<CourseDto> serviceResponse = courseService.save(courseDto);
            if (serviceResponse.isSuccess()) {
                response.success(serviceResponse.getMessage());
            } else {
                response.error(serviceResponse.getMessage());
            }
        } else {
            response.error(VALIDATION_ERROR_MESSAGE);
        }
        return response;
    }

    @RequestMapping(value = "/delete/{courseId}", method = RequestMethod.GET)
    public String delete(@PathVariable("courseId") long courseId) throws Exception {
        ServiceResponse<CourseDto> serviceResponse = courseService.delete(courseId);
        return INDEX_WITH_STATUS_REDIRECT_URL + encodeMessage(serviceResponse.getMessage());
    }
}
