package keith.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Controller
@RequestMapping("/home")
public class HomeController
{

    @RequestMapping
    public String home()
    {
        return "home";
    }
}
