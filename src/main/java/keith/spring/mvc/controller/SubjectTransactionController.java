package keith.spring.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import keith.api.maintenance.service.SubjectService;
import keith.domain.dto.ServiceResponse;
import keith.domain.dto.SubjectDto;
import keith.domain.dto.UIResponse;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Controller
@RequestMapping("/maintenance/subjecttransaction")
public class SubjectTransactionController {
    
    private static final String INDEX_VIEW = "maintenance/subject/transaction";

    @Autowired
    private SubjectService subjectService;

    @RequestMapping
    public String index(Model model) throws Exception {
        ServiceResponse<List<SubjectDto>> serviceResponse = subjectService.retrieveAll();
        if (serviceResponse.isSuccess()) {
            model.addAttribute("subjects", serviceResponse.getData());
        } else {
            model.addAttribute("statusMessage", serviceResponse.getMessage());
        }
        return INDEX_VIEW;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody UIResponse save(@RequestBody List<SubjectDto> subjectDtos) throws Exception {
        UIResponse response = new UIResponse();
        ServiceResponse<List<SubjectDto>> serviceResponse = subjectService.saveAll(subjectDtos);
        if (serviceResponse.isSuccess()) {
            response.success(serviceResponse.getMessage());
        } else {
            response.error(serviceResponse.getMessage());
        }
        return response;
    }
}
