package keith.spring.mvc.controller;

import static keith.domain.util.KeithUtils.encodeMessage;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import keith.api.maintenance.service.StudentService;
import keith.domain.dto.ServiceResponse;
import keith.domain.dto.StudentDto;
import keith.domain.dto.UIResponse;
import keith.spring.mvc.validation.StudentDtoValidator;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Controller
@RequestMapping("/maintenance/student")
public class StudentMaintenanceController {
    
    private static final String INDEX_VIEW = "maintenance/student/index";
    private static final String ADD_VIEW = "maintenance/student/add";
    private static final String EDIT_VIEW = "maintenance/student/edit";
    private static final String INDEX_WITH_STATUS_REDIRECT_URL = "redirect:/maintenance/student?status_message=";

    private static final String VALIDATION_ERROR_MESSAGE = "Validation Error";

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentDtoValidator studentDtoValidator;

    @InitBinder("studentForm")
    protected void initBinderStudentForm(WebDataBinder binder) {
        binder.addValidators(studentDtoValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(@RequestParam(value = "status_message", required = false) String statusMessage, Model model)
            throws Exception {
        if (statusMessage != null) {
            model.addAttribute("statusMessage", statusMessage);
        }
        ServiceResponse<List<StudentDto>> serviceResponse = studentService.retrieveAll();
        if (serviceResponse.isSuccess()) {
            model.addAttribute("students", serviceResponse.getData());
        } else {
            model.addAttribute("statusMessage", serviceResponse.getMessage());
        }
        return INDEX_VIEW;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) throws Exception {
        ServiceResponse<StudentDto> serviceResponse = studentService.retrieveNew();
        if (serviceResponse.isSuccess()) {
            model.addAttribute("studentForm", serviceResponse.getData());
        } else {
            model.addAttribute("statusMessage", serviceResponse.getMessage());
        }
        return ADD_VIEW;
    }

    @RequestMapping(value = "/edit/{studentId}", method = RequestMethod.GET)
    public String edit(@PathVariable("studentId") long studentId, Model model) throws Exception {
        ServiceResponse<StudentDto> serviceResponse = studentService.retrieveOne(studentId);
        if (serviceResponse.isSuccess()) {
            StudentDto studentDto = serviceResponse.getData();
            model.addAttribute("studentForm", studentDto);
            return EDIT_VIEW;
        } else {
            return INDEX_WITH_STATUS_REDIRECT_URL + encodeMessage(serviceResponse.getMessage());
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody UIResponse save(@ModelAttribute("studentForm") @Validated StudentDto studentDto,
            BindingResult bindingResult) throws Exception {
        UIResponse response = new UIResponse();
        if (!bindingResult.hasErrors()) {
            ServiceResponse<StudentDto> serviceResponse = studentService.save(studentDto);
            if (serviceResponse.isSuccess()) {
                response.success(serviceResponse.getMessage());
            } else {
                response.error(serviceResponse.getMessage());
            }
        } else {
            response.error(VALIDATION_ERROR_MESSAGE);
        }
        return response;
    }

    @RequestMapping(value = "/delete/{studentId}", method = RequestMethod.GET)
    public String delete(@PathVariable("studentId") long studentId) throws Exception {
        ServiceResponse<StudentDto> serviceResponse = studentService.delete(studentId);
        return INDEX_WITH_STATUS_REDIRECT_URL + encodeMessage(serviceResponse.getMessage());
    }
}
