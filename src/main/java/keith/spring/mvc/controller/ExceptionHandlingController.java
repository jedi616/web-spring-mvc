package keith.spring.mvc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import keith.domain.dto.UIResponse;

/**
 * Commit Date:
 * All errors/exceptions thrown by or through any of the Controllers which could possibly be thrown from any of the layers
 * behind it (service, dao, mapper, domain, util & etc.). All these errors/exceptions will be caught by this controller and returned to the
 * browser as having "500 Internal Server Error" Status Code.
 * 
 * @author Keith F. Jayme
 *
 */
@Controller
@ControllerAdvice
public class ExceptionHandlingController
{
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public @ResponseBody UIResponse exceptionHandler(Exception e)
    {
        UIResponse response = new UIResponse();
        response.error(e);
        return response;
    }
}
