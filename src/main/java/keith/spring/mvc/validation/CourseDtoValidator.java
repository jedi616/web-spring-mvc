package keith.spring.mvc.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import keith.domain.dto.CourseDto;

/**
 * 
 * @author Keith F. Jayme
 *
 *         Refer to "http://docs.spring.io/spring/docs/3.2.0.RELEASE/javadoc-api/org/springframework/validation/Validator.html" for sample
 *         code.
 * 
 */
@Component
public class CourseDtoValidator implements Validator
{
    @Override
    public boolean supports(Class<?> clazz)
    {
        return CourseDto.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors)
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseName", "field.required");
    }
}
