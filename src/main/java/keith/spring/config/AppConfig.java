package keith.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 
 * @author kjayme
 *
 */
@Configuration
@Import(value = { ServiceConfig.class, ServiceConfigTest.class })
public class AppConfig
{
}
